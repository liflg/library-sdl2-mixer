#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    echo "5): i386-darwin-macos"
    echo "6): x86_64-darwin-macos"
    echo "7): universal-darwin-macos (fat binary i386 and x86_64)"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    elif [ x"$SELECTEDOPTION" = x"5" ]; then
        MULTIARCHNAME=i386-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"6" ]; then
        MULTIARCHNAME=x86_64-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"7" ]; then
        MULTIARCHNAME=universal-darwin-macos
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-ggdb"
        export CXXFLAGS="-ggdb"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo "Optimization level '$OPTIMIZATION' is not yet implemented!"
        exit 1
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( OGGDIR="${PWD}/../library-ogg/${MULTIARCHNAME}"
      SDL2DIR="${PWD}/../library-sdl2/${MULTIARCHNAME}"
      VORBISDIR="${PWD}/../library-vorbis/${MULTIARCHNAME}"
      export CFLAGS="-I${OGGDIR}/include -I${VORBISDIR}/include"
      export LDFLAGS="-L${OGGDIR}/lib -L${VORBISDIR}/lib"
      export SDL_CFLAGS="-I${SDL2DIR}/include/SDL2 -D_REENTRANT"
      export SDL_LIBS="-L${SDL2DIR}/lib -lSDL2 -lpthread"
      cd "$BUILDDIR"
      ../source/configure \
         --prefix="$PREFIXDIR" \
         --disable-smpegtest \
         --disable-sdltest \
         --enable-music-ogg \
         --enable-music-wave \
         --disable-music-mp3 \
         --disable-music-flac \
         --disable-music-midi \
         --enable-static=false \
         --disable-music-mod
      make -j "$(nproc)"
      make install
      rm -rf "$PREFIXDIR"/{lib/*.la,lib/pkgconfig})

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}
darwin_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-g"
        export CXXFLAGS="-g"
    fi

    case "$MULTIARCHNAME" in
    i386-darwin-macos)
        export CFLAGS="$CFLAGS -arch i386"
        export CXXFLAGS="$CXXFLAGS -arch i386"
        export LDFLAGS="$LDFLAGS -arch i386"
        ;;
    x86_64-darwin-macos)
        export CFLAGS="$CFLAGS -arch x86_64"
        export CXXFLAGS="$CXXFLAGS -arch x86_64"
        export LDFLAGS="$LDFLAGS -arch x86_64"
        ;;
    universal-darwin-macos)
        export CFLAGS="$CFLAGS  -arch i386 -arch x86_64"
        export CXXFLAGS="$CXXFLAGS  -arch i386 -arch x86_64"
        export LDFLAGS="$LDFLAGS  -arch i386 -arch x86_64"
        ;;
    *)
        echo "$MULTIARCHNAME is not (yet) supported by this script."
        exit 1;;
    esac

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( OGGDIR="${PWD}/../library-ogg/${MULTIARCHNAME}"
      SDL2DIR="${PWD}/../library-sdl2/${MULTIARCHNAME}"
      VORBISDIR="${PWD}/../library-vorbis/${MULTIARCHNAME}"
      export CFLAGS="${CFLAGS} -I${OGGDIR}/include -I${VORBISDIR}/include -stdlib=libc++"
      export LDFLAGS="${LDFLAGS} -L${OGGDIR}/lib -L${VORBISDIR}/lib -stdlib=libc++"
      export SDL_CFLAGS="${CFLAGS} -I${SDL2DIR}/include/SDL2 -D_REENTRANT"
      export SDL_LIBS="-L${SDL2DIR}/lib -lSDL2"
      cd "$BUILDDIR"
      ../source/configure \
         --prefix="$PREFIXDIR" \
         --disable-smpegtest \
         --disable-sdltest \
         --enable-music-ogg \
         --enable-music-wave \
         --disable-music-mp3 \
         --disable-music-flac \
         --disable-music-midi \
         --enable-static=false \
         --disable-music-mod \
         --enable-music-ogg-shared=no

      make -j "$(getconf _NPROCESSORS_ONLN)"
      make install
      rm -rf "$PREFIXDIR"/{lib/*.la,lib/pkgconfig})

      install_name_tool -id @executable_path/../Frameworks/libSDL2_mixer-2.0.0.dylib "$PREFIXDIR"/lib/libSDL2_mixer-2.0.0.dylib

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
i386-darwin-macos)
    darwin_build;;
x86_64-darwin-macos)
    darwin_build;;
universal-darwin-macos)
    darwin_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING.txt "$PREFIXDIR"/lib/LICENSE-sdl2-mixer.txt

rm -rf "$BUILDDIR"

echo "SDL2_mixer for $MULTIARCHNAME is ready."
